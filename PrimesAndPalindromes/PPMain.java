import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Scanner;

public class PPMain {

	public static void main(String[] args) {

		Scanner sc= new Scanner(System.in);
		ArrayList<Integer> list = new ArrayList<Integer>();
		System.out.println("Enter the list of numbers on a single line: ");
		String i = sc.nextLine();
		for (String s : i.split("\\s"))  
		{  
			list.add(Integer.parseInt(s));  
		}

		System.out.println("Largest number: " + Collections.max(list));
		System.out.println("Smallest number: " + Collections.min(list));

		Iterator<Integer> it1 = isPalindrome(list).iterator();
		if (it1.hasNext()) {
			System.out.print("Palindromes: " +it1.next());
			while (it1.hasNext())
				System.out.print(", "+it1.next());
		}
		System.out.println();

		Iterator<Integer> it2 = isPrime(list).iterator();
		if (it2.hasNext()) {
			System.out.print("Primes: " +it2.next());
			while (it2.hasNext())
				System.out.print(", "+it2.next());
		}

	}	

	public static ArrayList<Integer> isPalindrome(ArrayList<Integer> list){
		ArrayList<Integer> palindromes = new ArrayList<Integer>();

		for(Integer i: list){
			String number= Integer.toString(i);
			String reverse= new StringBuilder(number).reverse().toString();
			if(number.equals(reverse)){
				palindromes.add(i);
			}
		}
		return palindromes;
	}

	public static ArrayList<Integer> isPrime(ArrayList<Integer> list){
		ArrayList<Integer> primes = new ArrayList<Integer>();

		for(Integer i: list){
			boolean isPrimeNumber= true;
			for (int j = 2; j < i/2; j++) {
				if (i % j == 0) {
					isPrimeNumber= false;
					break;
				}
			}
			if (isPrimeNumber)
			{
				primes.add(i);
			}		
		}
		return primes;
	}
}
