import java.util.Scanner;

public class GeometryMain {

	public static void main(String[] args) {

		getMenu();

	}

	public static void getMenu(){	
		System.out.println("Chose the Shape (Enter 1, 2 or 3):\n");
		System.out.println("1. Calculate the Area and Perimeter of a Circle");
		System.out.println("2. Calculate the Area and Perimeter of a Rectangle");
		System.out.println("3. Calculate the Area and Perimeter of a Triangle");

		int choice;
		Scanner s= new Scanner(System.in);
		choice = s.nextInt();

		switch(choice){
		case 1 :{
			System.out.println("Insert value for the Circle's radius: ");
			double radius =s.nextDouble();
			Shape circle = new Circle(radius);
			System.out.println("Circle radius: " + radius
					+ "\nCircle's Area is : " + circle.area()
					+ "\nCircle's Perimeter is: " + circle.perimeter());
			break;
		}
		case 2 :{
			System.out.println("Insert values for the Rectangle's width and length: ");
			double width = s.nextDouble();
			double length= s.nextDouble();
			Shape rectangle = new Rectangle(width, length);
			System.out.println("\nRectangle's Area is: " + rectangle.area()
			+ "\nRectangle's Perimeter: " + rectangle.perimeter());
			break;
		}
		case 3: {
			System.out.println("Insert values for the Triangle's sides: ");
			double a = s.nextDouble(); 
			double b = s.nextDouble();
			double c = s.nextDouble();
			Shape triangle = new Triangle(a,b,c);
			System.out.println("\nTringle's Area is: " + triangle.area()
			+ "\nTringle's Perimeter is: " + triangle.perimeter());
			break;
		}
		default: {
			System.out.println("Not a valid value");
			break;
		}
		}
		s.close();
	}
}

