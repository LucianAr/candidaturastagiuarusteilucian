public class Circle extends Shape{
	
	private final double radius;
	final double pi = Math.PI;
	
	public Circle(double radius){
		this.radius= radius;
	}

	@Override
	public double area() {
		// area of circle = π r^2
		// π = 3.14 and r is the radius of circle 
		return pi * Math.pow(radius, 2);
	}

	@Override
	public double perimeter() {
		// circumference of circle = 2πr
		return 2 * pi * radius;
	}
}
