public class Triangle extends Shape{
	 
	private final double a, b, c;
	 
	 public Triangle(double a, double b, double c){
		 this.a= a;
		 this.b= b;
		 this.c= c;	 
	 }

	@Override
	public double area() {
		// area of triangle = squareRoot(s(s - a) (s - b) (s - c)) 
		// s = 1/2 (a + b + c) 
		double s;
		s= (a+ b+ c)/2;
		return Math.sqrt(s * (s - a) * (s - b) * (s - c));
	}

	@Override
	public double perimeter() {
		// perimeter of triangle = (a + b + c)
		return a + b + c;
	}
}
