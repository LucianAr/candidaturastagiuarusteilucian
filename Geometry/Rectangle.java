public class Rectangle extends Shape{
	
	private final double width, length;
	
	public Rectangle(double width, double length){
		this.width= width;
		this.length= length;
	}

	@Override
	public double area() {
		//area of a rectangle = length * width
		return length * width ;
	}

	@Override
	public double perimeter() {
		//perimeter of rectangle = 2(length + width)
		return 2*(length + width);
	}
}
